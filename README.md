# Unqork Ag-Grid POC

A POC for utilizing ag-Grid to map the form builder sheet component configurations/validations to a dynamic grid on the client form view.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn storybook`

Runs the app with storybook.<br />
Open [http://localhost:9009](http://localhost:9009) to view it in the browser.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!