import React from 'react';
import { storiesOf } from '@storybook/react';
import UnqorkAgGrid from '../UnqorkAgGrid';

export default {
  title: 'Unqork ag-Grid',
};

storiesOf("Unqork AgGrid", module) 
  .add('Default', () => <UnqorkAgGrid />)