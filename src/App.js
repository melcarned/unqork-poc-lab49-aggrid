import React from 'react';
import UnqorkAgGrid from './UnqorkAgGrid';
import './App.css';

const App = () => {
  return (
    <div className="App">
        <UnqorkAgGrid />
    </div>
  );
}

export default App;
