import React from 'react';
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { GRID_OPTIONS, COLUMN_DEFS, ROW_DATA } from './config';

const UnqorkAgGrid = ({ rowData = ROW_DATA }) => {
  return (
    <div
      className="ag-theme-balham"
      style={{
        height: '500px',
        width: '1000px'
      }}
    >
      <AgGridReact
        gridOptions={GRID_OPTIONS}
        columnDefs={COLUMN_DEFS}
        rowData={rowData}>
      </AgGridReact>
    </div>
  );
}

export default UnqorkAgGrid;