export const ROW_DATA = [
  {
    symbol: "SHOP",
    shares: 35,
    price: 150,
    date: '02-15-2019',
    status: 'APPROVED'
  }, 
  {
    symbol: "AMZN",
    shares: 35000,
    price: 1500,
    date: '06-18-2019',
    status: 'APPROVED'
  }, 
  {
    symbol: "NVDA",
    shares: 100,
    price: 500,
    date: '07-16-2019',
    status: 'APPROVED'
  },
  {
    symbol: "ANET",
    shares: 10,
    price: 240,
    date: '04-01-2019',
    status: 'PENDING'
  },
  {
    symbol: "CMG",
    shares: 65,
    price: 130,
    date: '05-21-2019',
    status: 'APPROVED'
  },
  {
    symbol: "GOOG",
    shares: 55,
    price: 1900,
    date: '08-14-2019',
    status: 'APPROVED'
  }
];

export default ROW_DATA;