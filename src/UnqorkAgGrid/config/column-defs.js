export const COLUMN_DEFS = [
  {
    headerName: "Symbol",
    field: "symbol"
  }, 
  {
    headerName: "Shares",
    field: "shares"
  }, 
  {
    headerName: "Price",
    field: "price"
  },
  {
    headerName: "Date",
    field: "date"
  },
  {
    headerName: "Status",
    field: "status",
    editable: true
  }
];